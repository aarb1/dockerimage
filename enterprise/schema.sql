-- This create database with initial database
create database phone;
use phone;
create table numbers (
  name varchar(50),
  number varchar(30)
);
insert into numbers values('Arnold Smith', '555-111-2232');
insert into numbers values('Jake Jones', '222-241-2115');
insert into numbers values('Ruth Sith', '123-555-1117');
